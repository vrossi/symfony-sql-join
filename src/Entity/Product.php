<?php

namespace App\Entity;

class Product {
public $id;
public $name;
public $description;
public $price;


public function fromSQL(array $sql) {
  $this->id = $sql["id"];
  $this->name = $sql["name"];
  $this->description = $sql["description"];
  $this->price = $sql["price"];
 
  
}

}