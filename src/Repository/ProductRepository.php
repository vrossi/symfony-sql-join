<?php

namespace App\Repository;

use App\Entity\Product;


class ProductRepository
{

  public function add(Product $product)
  {

    try {
      $cnx = new \PDO("mysql:host={$_ENV["MYSQL_HOST"]}:3306;dbname={$_ENV["MYSQL_DATABASE"]}", $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);

      $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

      $query = $cnx->prepare("INSERT INTO product (name, description, price) VALUES (:name, :description, :price)");

      $query->bindValue(":name", $product->name);
      $query->bindValue(":description", $product->description);
      $query->bindValue(":price", $product->price);

      $query->execute();

      $product->id = intval($cnx->lastInsertId());

    } catch (\PDOException $e) {
      dump($e);
    }
  }


  public function get(int $id) {
    return $id = new Product();
  }

  public function getAll() : array
  {

    $products = [];
    try {

      $cnx = new \PDO("mysql:host={$_ENV["MYSQL_HOST"]}:3306;dbname={$_ENV["MYSQL_DATABASE"]}", $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);
      $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

      $query = $cnx->prepare("SELECT * FROM product");

      $query->execute();


      foreach ($query->fetchAll() as $row) {
        $product = new Product();
        $product->fromSQL($row);
        $products[] = $product;
      }

    } catch (\PDOException $e) {
      dump($e);
    }
    return $products;
  }


}